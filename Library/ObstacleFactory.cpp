#include "ObstacleFactory.h"
#include "Plus.h"
#include "Minus.h"

// Fungsi static yang digunakan untuk membuat obstacle sesuai nama yang ada.
std::shared_ptr<Obstacle> ObstacleFactory::DrawObstacle(std::string name)
{
	Obstacle * instance = nullptr;

	if (name == "Plus")
		instance = new Plus();
	else if (name == "Minus")
		instance = new Minus();

	if (instance != nullptr)
		return std::shared_ptr<Obstacle>(instance);
	else
		return nullptr;
}
