#pragma once
#include <iostream>

// Class Obstacle merupakan Interface dari Obstacle
class Obstacle
{
public:
	virtual void draw() = 0; // Fungsi virtual yang digunakan untuk menampilkan obstacle.
};
