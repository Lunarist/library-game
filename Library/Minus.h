#pragma once
#include "Obstacle.h"

// Class Minus merupakan turunan dari Class Obstacle yang merupakan Obstacle yang dapat membuat pemain kalah.
class Minus : public Obstacle
{
private:
	char character = '-'; // Variabel bentuk dari character Obstacle Minus.

public:
	void draw() override; // Fungsi turunan dari Obstacle yang digunakan untuk menampilkan Obstacle Minus.
};

