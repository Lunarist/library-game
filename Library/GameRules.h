#pragma once
#include "Player.h"
#include "Map.h"
#include "ObstacleFactory.h"

// Class GameRules merupakan class yang berisi segalanya tentang peraturan dari game.
class GameRules
{
private:
	Player *player; // Variabel yang diambil dari class Player.
	Map *map; // Variabel yang diambil dari class Map.
	ObstacleFactory *of; // Variabel yang diambil dari class ObstacleFactory.

public:
	GameRules(Player *, Map *); // Constructor overload dari class GameRules dengan 2 parameter, yaitu variabel dari class Player dan variabel dari class Map.
	void play(); // Fungsi yang menjalankan game(menampilkan map dan player).
	void control(); // Fungsi yang mengatur pergerakan dari player.
	void spawnObstacle(std::string obs, int posX, int posY); // Fungsi yang digunakan untuk memanggil obstacle.
	bool checkLose(); // Fungsi yang digunakan untuk mengecek apakah permainan telah selesai (kalah) atau belum.
};

