#pragma once
#include "Obstacle.h"

// Class Plus merupakan turunan dari Class Obstacle yang merupakan Obstacle yang dapat menambah score pemain.
class Plus : public Obstacle
{
private:
	char character = '+'; // Variabel bentuk dari character Obstacle Plus.

public:
	void draw() override; // Fungsi turunan dari Obstacle yang digunakan untuk menampilkan Obstacle Plus.
};

