#pragma once
#include "Obstacle.h"
#include <memory>

// Class ObstacleFactory merupakan Factory Pattern untuk Obstacle yang digunakan untuk membuat Obstacle sebanyak yang diperulkan
class ObstacleFactory
{
public:
	static std::shared_ptr<Obstacle> DrawObstacle(std::string name); // Fungsi static yang digunakan untuk membuat obstacle sesuai nama yang ada.
};
