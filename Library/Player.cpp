#include "Player.h"
#include <windows.h>

HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
COORD CursorPosition;

// Fungsi untuk mengatur letak output saat program dijalankan.
void gotoxy(int x, int y)
{
	CursorPosition.X = x;
	CursorPosition.Y = y;
	SetConsoleCursorPosition(console, CursorPosition);
}

// Constructor dari class Player.
Player::Player()
{
	x = 5;
	y = 5;
}

// Constructor overload dari class Player yang memiliki 2 parameter untuk posisi x dan y character.
Player::Player(int x, int y)
{
	this -> x = x;
	this -> y = y;
}

// Fungsi yang digunakan untuk mengatur posisi dari character berdasarkan parameter x dan y.
void Player::setPosition(int x, int y)
{
	this -> x = x;
	this -> y = y;
}

// Fungsi untuk mendapatkan posisi x character.
int Player::getX()
{
	return x;
}

// Fungsi untuk mendapatkan posisi y character.
int Player::getY()
{
	return y;
}

// Fungsi yang berisi pergerakan character ke atas.
void Player::moveUp()
{
	y--;
}

// Fungsi yang berisi pergerakan character ke bawah.
void Player::moveDown()
{
	y++;
}

// Fungsi yang berisi pergerakan character ke kanan.
void Player::moveRight()
{
	x++;
}

// Fungsi yang berisi pergerakan character ke kiri.
void Player::moveLeft()
{
	x--;
}

// Fungsi untuk menggambarkan posisi character dengan posisi x dan y yang baru.
void Player::draw()
{
	gotoxy(x, y);
	std::cout << character;
}

// Fungsi untuk menghapuskan  character dari posisi x dan y yang lama.
void Player::erase()
{
	gotoxy(x, y);
	std::cout << " ";
}